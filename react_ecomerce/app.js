const express = require('express')
const mongoose = require('mongoose')
const morgan = require("morgan");
const bodyParser = require('body-parser')
const cookieParser = require("cookie-parser")
const cors = require("cors");
const expressValidator = require('express-validator')
require('dotenv').config();
const app = express()
//import routes

const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const categoryRoutes = require('./routes/category');
const productRoutes = require('./routes/product');
//app


//db  connnection
mongoose.connect(process.env.DATABASE,{
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology: true

}).then(()=>console.log('DB Connected'))


//Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());

//Routes
app.use('/api',authRoutes);
app.use('/api',userRoutes);
app.use('/api',categoryRoutes);
app.use('/api',productRoutes);


//Server Start
const port = process.env.PORT || 8001
app.listen(port,()=>{
    console.log(`Server runing on port ${port}`)
})