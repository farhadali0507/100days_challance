const User = require('../models/user');
const jwt = require('jsonwebtoken'); // to generate signin token
const expressjwt = require('express-jwt'); // for authorization check
const { errorHandler } = require("../helpers/dbErrorHandler");


exports.signup = (req, res)=>{
   // console.log('req.body',req.body)
    const user = new User(req.body);
    user.save((error, user)=>{
        if(error){
            return res.status(400).json({
                error : errorHandler(error)
            })
        }

        user.salt = undefined;
        user.hashed_password = undefined;

        res.json({
            user
        })
    })
    // return res.json({
    //     message: "signup page Here"
    // })
}


exports.signin = (req, res)=>{
    //find the user base on email
    const {email, password} = req.body;
   
    User.findOne({email}, (error,user)=>{
        console.log(user)
        if(error || !user){
            return res.status(400).json({
                error: "User with that email does not exist Please Signup"
            })
        }

        //if user is found make sure the email and password match
        //Create authenticate metho in user model
       
        //Generate a sign token uthe user and secret
        if(!user.authenticate(password)){
            return res.status(400).json({
                error: " Email and password dont match"
            })
        }

        const token = jwt.sign({_id: user._id}, process.env.JWT_SECRET );
       //persit  the token as 't' in cookie with expiray date
        res.cookie('t', token, {expire : new Date()+ 9999 } )

        //return response with user and token to frontend client
        const { _id,name, email, role } = user;
        return res.json({ token, user:{_id, name, email, role} })

 
     } )
   
}

exports.signout = (req, res)=>{
    res.clearCookie('t');
    res.json({message: " Signout Success"})
};


exports.requireSignin = expressjwt({
    secret: process.env.JWT_SECRET,
    userProperty: "auth",
    algorithms: ['HS256']
})


exports.isAuth = (req, res, next)=>{
    let user = req.profile && req.auth && req.profile._id == req.auth._id;
    if(!user){
        return res.status(403).json({
            error: "Access defied"
        })
    }
    next();
}

exports.isAdmin = (req, res, next)=>{
    if(req.profile.role ===0){
        return res.status(403).json({
            error: "Admin recorse! Access denied"
        })
    }
    next();
}

